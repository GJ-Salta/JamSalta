﻿#pragma strict

var divFacter : float = 2;
var customName : boolean = false;
var propName1 : String;
var propName2 : String;


function Start () {
    if(customName == false){
        GetComponent.<Renderer>().material.mainTextureScale.x = transform.localScale.x/divFacter;
        GetComponent.<Renderer>().material.mainTextureScale.y = transform.localScale.y/divFacter;
    }
    else{
        var scale : Vector2= Vector2( transform.localScale.x/divFacter, transform.localScale.z/divFacter );
        GetComponent.<Renderer>().material.SetTextureScale(propName1,scale);
        GetComponent.<Renderer>().material.SetTextureScale(propName2,scale);
    }
}