﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMovement : MonoBehaviour {

    private Transform platform;
	private Rigidbody rb;
    public float Speed;
	public float MaxSpeed;
    private float yMin, yMax;
	public float range;
	private Vector3 reset;
	// Use this for initialization
	void Start ()
    {
        platform = GetComponent<Transform>();
		rb = GetComponent<Rigidbody> ();
		reset = platform.position;
        yMin = platform.position.y;
        yMax = yMin + range;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
		if (platform.position.y >= yMax) {
			platform.position = reset;
		} else {
			rb.AddForce(Vector3.up * Speed);
		}
		if (rb.velocity.y > MaxSpeed) {
			rb.velocity = new Vector3 (rb.velocity.x, MaxSpeed, rb.velocity.z);
		}
        //platform.Translate(new Vector3(0, speed, 0));
    }



}
