﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movement : MonoBehaviour {
	private AudioSource au;
    private Rigidbody rb;
    private Animator anim;
	private VirtualJoystick joystick;
	public string Nivel;

    public float Speed;
	public float MaxSpeed;
	public float JumpSpeed;

    public bool isJumping;

	public Transform t;
    public bool Detect = false;
	public bool Quieto = false;

	public AudioClip Salto;
	public AudioClip MuerteS;

    // Use this for initialization
    void Start()
    {
		au = GetComponent<AudioSource> ();
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
		joystick = GameObject.FindGameObjectWithTag ("Joystick").GetComponent<VirtualJoystick> ();
        isJumping = false;
    }

    // Update is called once per frame
    void Update () {
		/*
		if (!Quieto) {
			if (Detect) {
				RaycastHit hit;
				if (Physics.Raycast (t.position, -Vector3.up, out hit, 0.5f)) {
					if (hit.collider.tag == "Terrain") {
						isJumping = false;
						anim.SetBool ("IsJump", false);
					}
				}
			}
			float moveForward = Input.GetAxis ("Horizontal");
			Vector3 movement = new Vector3 (moveForward, 0.0f, 0.0f);
			if (Input.GetKey (KeyCode.RightArrow)) {
				player.gameObject.transform.localScale = new Vector3 (1, 1, 1);
				anim.SetBool ("IsRunning", true);
			} else if (Input.GetKey (KeyCode.LeftArrow)) {
				player.gameObject.transform.localScale = new Vector3 (-1, 1, 1);
				anim.SetBool ("IsRunning", true);
			} else {
				anim.SetBool ("IsRunning", false);
				Vector3 v = new Vector3 (0, player.velocity.y, 0);
				player.velocity = v;
			}

			if (Input.GetKeyDown (KeyCode.UpArrow) & !isJumping) {
				//float doJump = Input.GetAxis("Vertical");
				// Vector3 jump = new Vector3(0.0f, doJump, 0.0f);
				player.AddForce (Vector3.up * 3000f * Time.deltaTime, ForceMode.Impulse);
				isJumping = true;
				Detect = false;
				Invoke ("Detection", 0.5f);
				anim.SetBool ("IsJump", true);

				au.clip = Salto;
				au.Play ();
			}

			//if (speed > 50f) speed = 15f;
			//personaje.Translate(movement * speed);
			if (!isJumping) {
				player.AddForce (movement * speed * Time.deltaTime);
			} else {
				player.AddForce (movement * (speed / 2) * Time.deltaTime);
			}
		}
		*/
	}

	void FixedUpdate()
	{
		if (!Quieto) {
			
			RaycastHit hit;
			if (Physics.Raycast (t.position, -Vector3.up, out hit, 0.15f)) {
				if (hit.collider.tag == "Terrain") {
					if (Detect) {
						isJumping = false;
						anim.SetBool ("IsJump", false);
					} else {
						Detect = true;
					}
				}
			} else {
				isJumping = true;
				Detect = true;
			}

			float JumpF = Input.GetAxis ("Vertical");
			if (JumpF > 0 & !isJumping) {
				
				rb.AddForce (Vector3.up * JumpSpeed, ForceMode.Impulse);
				isJumping = true;
				Detect = false;
				anim.SetBool ("IsJump", true);

				au.clip = Salto;
				au.Play ();
			}


			if (rb.velocity.x < -MaxSpeed) {
				rb.velocity = new Vector3 (-MaxSpeed, rb.velocity.y, rb.velocity.z);
			}
			if (rb.velocity.x > MaxSpeed) {
				rb.velocity = new Vector3 (MaxSpeed, rb.velocity.y, rb.velocity.z);
			}
			float moveForward = Input.GetAxis ("Horizontal");
			moveForward = joystick.Horizontal ();
			if (moveForward > 0) {
				rb.gameObject.transform.localScale = new Vector3 (1, 1, 1);
				anim.SetBool ("IsRunning", true);
			} else if (moveForward < 0) {
				rb.gameObject.transform.localScale = new Vector3 (-1, 1, 1);
				anim.SetBool ("IsRunning", true);
			} else {
				anim.SetBool ("IsRunning", false);
				float z = Mathf.Lerp (rb.velocity.x, 0, 5f);
				rb.velocity = new Vector3 (z, rb.velocity.y, rb.velocity.z);
			}
			Vector3 movement = new Vector3 (moveForward, 0.0f, 0.0f);
			if (!isJumping) {
				rb.AddForce (movement * Speed);
			} else {
				rb.AddForce (movement * (Speed / 2));
			}
			rb.AddForce (movement * Speed);

		}
	}

	public void Muerte()
	{
		au.clip = MuerteS;
		au.Play ();
		Quieto = true;
		rb.useGravity = false;
		rb.velocity = Vector3.zero;
		GetComponent<SpriteRenderer> ().enabled = false;
		Invoke ("ResetLevel", 3);
	}

	void ResetLevel()
	{
		SceneManager.LoadScene(Nivel, LoadSceneMode.Single);
	}

	public void JumpButton()
	{
		if (!Quieto) {
			if (!isJumping) {
				rb.AddForce (Vector3.up * JumpSpeed, ForceMode.Impulse);
				isJumping = true;
				Detect = false;
				anim.SetBool ("IsJump", true);

				au.clip = Salto;
				au.Play ();
			}
		}
	}
}
