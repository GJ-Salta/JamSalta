﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fondo : MonoBehaviour {

	public GameObject[] Objetos;
	public GameObject Piso;
	public Transform puntoCreacion;
	public int PisoZtile;
	public int PisoXtile;

	public int ObjZtile;
	public int ObjXtile;

	private int pisoactual;
	private int Objactual;
	void Start () {
		//GenerarObjetos ();
		GenerarPiso ();
	}

	void GenerarPiso()
	{
		for (int x = 0; x < PisoXtile; x++) {
			pisoactual = 50 * x;
			GameObject pisoX = Instantiate (Piso, transform.position + new Vector3 (pisoactual, 0, 0), Piso.transform.rotation) as GameObject; 
				
			for (int z = 0; z < PisoZtile; z++) {
				GameObject pisoZ = Instantiate (Piso, transform.position + new Vector3 (pisoactual, 0, 50 * z), Piso.transform.rotation) as GameObject; 
			}
		}
	}
	void GenerarObjetos()
	{
		for (int x = 0; x < ObjXtile; x++) {
			int a = Random.Range (-40, 40);
			Objactual = Random.Range (a * x, a * x);
			int randomObjX = Random.Range (0, Objetos.Length);
			GameObject objX = Instantiate (Objetos[randomObjX], puntoCreacion.position + new Vector3 (Objactual, 0, a), Objetos[randomObjX].transform.rotation) as GameObject;
			float randx = Random.Range (0, 360);
			objX.transform.eulerAngles = new Vector3(-90, randx, 0);
			if (randomObjX == 0) {
				objX.transform.localScale += new Vector3 (objX.transform.localScale.x + Random.Range (-20, 20), objX.transform.localScale.y + Random.Range (-50, 50), objX.transform.localScale.x + Random.Range (-50, 50));
			}
				for (int z = 0; z < PisoZtile; z++) {
				int c = Random.Range(-40, 40);
				int RandomZ = Random.Range (c * z, c * z);
				int randomObjZ = Random.Range (0, Objetos.Length);
				GameObject objZ = Instantiate (Objetos[randomObjZ], puntoCreacion.position + new Vector3 (Objactual + RandomZ, 0, RandomZ), Objetos[randomObjZ].transform.rotation) as GameObject;
				float randz = Random.Range (0, 360);
				objZ.transform.eulerAngles = new Vector3(-90, randz, 0);
				if (randomObjZ == 0) {
					objZ.transform.localScale += new Vector3 (objZ.transform.localScale.x + Random.Range (-20, 20), objZ.transform.localScale.y + Random.Range (-50, 80), objZ.transform.localScale.x + Random.Range (-50, 50));
				} else {
					objZ.transform.localScale += new Vector3 (objZ.transform.localScale.x + Random.Range (0.7f, 1f), objZ.transform.localScale.y + Random.Range (0.7f, 1f), objZ.transform.localScale.x + Random.Range (0.7f, 1f));
				}
			}
		}
	}
}
