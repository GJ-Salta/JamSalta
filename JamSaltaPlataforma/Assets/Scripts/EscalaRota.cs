﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscalaRota : MonoBehaviour {

	// Use this for initialization
	void Start () {
		transform.localScale += new Vector3 (transform.localScale.x + Random.Range (-20, 10), transform.localScale.y + Random.Range (-20, 20), transform.localScale.x + Random.Range (-20, 10));
		float randx = Random.Range (0, 360);
		transform.eulerAngles = new Vector3(-90, randx, 0);
	}
}
