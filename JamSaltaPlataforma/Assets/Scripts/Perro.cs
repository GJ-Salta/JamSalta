﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Perro : MonoBehaviour {

	private Animator anim;
	private Rigidbody rb;
	private GameObject pla;
	private bool SeMueve;
	private GameObject player;

	public float Speed;
	public float MaxSpeed;
	public float DistanciaMuerte;
	public bool SentidoDerecha;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		rb = GetComponent<Rigidbody> ();
		pla = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if (SeMueve) {
			if (SentidoDerecha) {
				rb.AddForce (Vector3.right * Speed);
				transform.localScale = new Vector3 (-1, 1, 1);
			} else {
				rb.AddForce (-Vector3.right * Speed);
				transform.localScale = new Vector3 (1, 1, 1);
			}

			if (rb.velocity.x < -MaxSpeed) {
				rb.velocity = new Vector3 (-MaxSpeed, rb.velocity.y, rb.velocity.z);
			}
			if (rb.velocity.x > MaxSpeed) {
				rb.velocity = new Vector3 (MaxSpeed, rb.velocity.y, rb.velocity.z);
			}

			float dist = Vector3.Distance (pla.transform.position, transform.position);
			if (dist < DistanciaMuerte) {
				player.GetComponent<Movement> ().Muerte ();
			}
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.transform.tag == "Player") {
			player = col.gameObject;
			Atacar ();
		}
	}

	void Atacar()
	{
		SeMueve = true;
		Destroy (gameObject, 5);
		anim.SetBool ("Atacar", true);
	}
}
