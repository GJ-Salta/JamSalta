﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinalNivel : MonoBehaviour {

	public string Nivel;
	// Use this for initialization
	void Start () {
		
	}


	void OnTriggerEnter(Collider col)
	{
		if (col.transform.tag == "Player") {
			SceneManager.LoadScene(Nivel, LoadSceneMode.Single);
		}
	}
}
