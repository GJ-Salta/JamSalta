﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoFrases : MonoBehaviour {

	private AudioSource au;
	public AudioClip[] Frases;
	int tiempo;

	// Use this for initialization
	void Start () {
		au = GetComponent<AudioSource> ();
		tiempo = Random.Range (10, 30);
		Invoke("Reproduccion", tiempo);
	}

	void Update()
	{
		if(Input.GetKey (KeyCode.Escape)) {
			Application.Quit ();
		}
	}

	void Reproduccion()
	{
		tiempo = Random.Range (10, 30);
		int fra = Random.Range (20, Frases.Length);
		au.clip = Frases [fra];
		au.Play ();
		Invoke("Reproduccion", tiempo);
	}
}
